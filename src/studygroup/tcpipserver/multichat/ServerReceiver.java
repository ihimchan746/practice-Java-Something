package studygroup.tcpipserver.multichat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class ServerReceiver {

  Socket socket;
  DataInputStream dataInputStream;
  DataOutputStream dataOutputStream;

  public ServerReceiver(Socket socket) {
    this.socket = socket;
    try {
      dataInputStream = new DataInputStream(socket.getInputStream());
      dataOutputStream = new DataOutputStream(socket.getOutputStream());
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
