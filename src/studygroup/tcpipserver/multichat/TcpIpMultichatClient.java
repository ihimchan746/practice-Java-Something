package studygroup.tcpipserver.multichat;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class TcpIpMultichatClient {

  public static void main(String[] args) {
    if (args.length != 1) {
      System.out.println("USAGE : Java IcpIpMultiChatClient 대화명");
      System.exit(0);
    }

    try {
//      String serverIp = "127.0.0.1";
      String serverIp = "127.0.0.1";
      int serverPort = 7777;

      Socket socket = new Socket(serverIp, serverPort);
      System.out.println("서버에 연결되었습니다.");
      Thread sender = new Thread(new ClientSender(socket, args[0]));
      Thread receiver = new Thread(new ClientReceiver(socket));

      sender.start();
      receiver.start();
    } catch (UnknownHostException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  static class ClientSender extends Thread {
    Socket socket;
    DataOutputStream out;
    String name;

    public ClientSender(Socket socket, String name) throws IOException {
      this.socket = socket;
      out = new DataOutputStream(socket.getOutputStream());
      this.name = name;
    }


  }

  static class ClientReceiver extends Thread {
    Socket socket;

    DataInputStream in;
    String name;

    public ClientReceiver(Socket socket) throws IOException {
      this.socket = socket;
      in = new DataInputStream(socket.getInputStream());
      this.name = name;
    }
  }

}
