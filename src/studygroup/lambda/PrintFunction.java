package studygroup.lambda;

@FunctionalInterface
public interface PrintFunction {

    public abstract void print();
}
