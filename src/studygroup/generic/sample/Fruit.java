package studygroup.generic.sample;

public class Fruit {

    String name;

    public Fruit() {}

    public Fruit(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
